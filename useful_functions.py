import time
import datetime as dt
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import statistics as stats
import requests

def allowance_calculater(startday, endday = '2020-12-25', rate = 36):
    try:
        date1 = time.strptime(startday,"%Y-%m-%d")
        date2 = time.strptime(endday,"%Y-%m-%d")
        date1=dt.date(date1[0],date1[1],date1[2])
        date2=dt.date(date2[0],date2[1],date2[2])
        time_delta = date2 - date1
    
        if  time_delta.days >= 0:
            print('THe living days have {} days.'.format(time_delta.days))
            print('The living allowance needed for this semester is: {} RMB.'.format((time_delta.days+1) * rate))
        else:
            print('Start day is too late!!!')
    except Exception as e:
        print(e)
        print('You must give the variable "startday" in the format of year-month-day!')


def working_sarlay_calculator(hours):
    try:
        h = int(hours)
        if h <= 36:
            pay = h * 120
            print('Your working payment should be: ', pay)
        else:
            pay = (h - 36) * 220 + 36 * 120
            print('Your working payment should be: ', pay)

    except Exception as e:
        print(e)
        print('Your input is wrong! Please try another input.')


def VWAP(df, col_name, col_weight):
    try:
        total_price = sum(df[col_name] * df[col_weight])
        total_volume = sum(df[col_weight])
        return total_price / total_volume
    except Exception as e:
        print('Wrong column name or Weight column name.')
        return None


def clean_mean(a:list):
    new_a = []
    for i in a:
        if type(i)==int:
            new_a.append(i)
        elif type(i)==str and i[0] != '$':
            new_a.append(int(i))
        else:
            new_a.append(int(i[1:]))
    return round(sum(new_a) / len(new_a), 2)

def file_word_count(file_path: str, seperater: str):
    word_dict = {}
    with open(file_path, 'r') as handle:
        for line in handle:
            line_list = line.split(seperater)
            for words in line_list:
                if words not in word_dict:
                    word_dict[words] = 1
                else:
                    word_dict[words] = word_dict[words] +1
    return word_dict


def VWAP_of_each(file_path: str, seperater: str):
    stock_dict = {}
    with open(file_path, 'r') as handle:
        handle.readline()
        for line in handle:
            line_list = line.split(seperater)
            if line_list[0] not in stock_dict:
                stock_dict[line_list[0]] = [[], []]
                stock_dict[line_list[0]][0].append(float(line_list[6]))
                stock_dict[line_list[0]][1].append(float(line_list[7]))
            else:
                stock_dict[line_list[0]][0].append(float(line_list[6]))
                stock_dict[line_list[0]][1].append(float(line_list[7]))
    # print(stock_dict)
    for stock, price_volome_list in stock_dict.items():
        total_vol = sum(price_volome_list[1])
        volumes = price_volome_list[1]
        prices = price_volome_list[0]
        if total_vol == 0: 
            total_vol = len(volumes)
            volumes = [1 for i in volumes]

        weights = [i / total_vol for i in volumes]
        stock_VWAP = sum([prices[i] * weights[i] for i in range(len(volumes))])

        print(stock, "'s VWAP is: ", stock_VWAP)


def yahoo_history_data(code_list: list, start: str, end: str):
    start_timestamp = int(time.mktime(time.strptime(start, "%Y-%m-%d")))
    end_timestamp = int(time.mktime(time.strptime(end, "%Y-%m-%d")))
    print(start_timestamp, end_timestamp)
    df_list = []
    for code in code_list:
        url = 'https://au.finance.yahoo.com/quote/%s/history?period1=%s&period2=%s&interval=1d&filter=history&frequency=1d' % (code,
                                                                                                                               start_timestamp, end_timestamp)
        while True:
            yahoo_hist_price_page = requests.get(url)
            status = yahoo_hist_price_page.status_code
            if status != 200:
                raise Exception('Some Error Happened. Http Error Code %d' % yahoo_hist_price_page.status_code)
                continue
            else:
                print('Success!!!')
                df = pd.read_html(yahoo_hist_price_page.text)[0]
                df = df.iloc[:-1]
                df_list.append(df)
                break
    return df_list


def netease_history_data(code_list: list):
#     start_timestamp = time.mktime(time.strptime(start, "%Y-%m-%d"))
#     end_timestamp = time.mktime(time.strptime(end, "%Y-%m-%d"))
    df_list = []
    for code in code_list:
        url = 'http://quotes.money.163.com/trade/lsjysj_{}.html?year=2019&season=4'.format(code)
        while True:
            netease_hist_price_page = requests.get(url)
            status = netease_hist_price_page.status_code
            if status != 200:
                raise Exception('Some Error Happened. Http Error Code %d' % netease_hist_price_page.status_code)
                continue
            else:
                print('Success!!!')
                df = pd.read_html(netease_hist_price_page.text)[3]
                df_list.append(df)
                break
    return df_list


def risk_free_rate_of_US(url, year): 
    url = url.format(year)
    ust_page = requests.get(url)
    if ust_page.status_code == 200:
        print('Successful Fetching')
        return pd.read_html(ust_page.text)[1]
    else:
        raise Exception('Some Error Happened. Http Error Code %d' % ust_page.status_code)
